#!/bin/bash
set -eu
source ./scripts/common.sh
export PATH=$PATH:$(pwd)

USER="Domain User"

# Run all scripts

# Provision the HSM
echo "*** TEST *** > Provisioning the HSM"
provisioning

# Generate the CA
echo "*** TEST *** > Generate the CA"
generate_ca "$USER" 

# Generate CSR
echo "*** TEST *** > Generate CSR"
create_csr "$USER"

# Sign CSR
echo "*** TEST *** > Sign CSR"
sign_csr "$USER"

# Sign EFI application
echo "*** TEST *** > Sign EFI"
sign_efi "$USER"

# Cleanup
rm -f csr.der priv.der cert.der efi.signed.bin