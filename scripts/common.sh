STHSM_PROVISION="provision"

function generate_ca() {
    USER="$1"; shift

    # Generate the CA
    ${STHSM_PROVISION} --testing --auth-key-label="${USER}" ca create \
      --common-name="Test CA" \
      --organization="Test Org" \
      --organizational-unit="Test unit" \
      --country="Test Country" \
      --locality="Test City" \
      --province="Test Province" \
      --street-address="Test Street 1234" \
      --postal-code="12345" \
      --out ca.der
}

function provisionedp () {
    # Check if the HSM is already provisioned
    if ${STHSM_PROVISION} pki list &> /dev/null; then
	return 0
    fi
    return 1
}

function provisioning () {
    # lookup sthsm-provision executable in PATH
    if ! command -v ${STHSM_PROVISION} &> /dev/null
    then
	echo "${STHSM_PROVISION} could not be found"
	exit 1
    fi
    
    if provisionedp; then
	echo "HSM is already provisioned"
    ${STHSM_PROVISION} --testing reset
    fi

    # Provision the HSM
    ${STHSM_PROVISION} pki provision --testing --config scripts/testdata/default.json
}

function sign_csr () {
    USER="$1"; shift

    # Sign the CSR
    ${STHSM_PROVISION} --testing --auth-key-label="${USER}" ca sign-csr \
      --csr csr.der \
      --ca-common-name="Test CA" \
      --out cert.der
}

function reset () {
    # Reset the HSM
    ${STHSM_PROVISION} --testing reset
}

function create_csr () {
     USER="$1"; shift

    # Generate the CSR
    ./sthsm-creds --testing --auth-key-label="${USER}" csr create \
    --common-name="Test" \
    --organization="Test Org" \
    --organizational-unit="Test Org Unit" \
    --country="Test Country" \
    --locality="Test City" \
    --province="Test Province" \
    --street-address="Test Street" \
    --postal-code="Test Postcode" \
    --key-algorithm="rsa2048" \
    --out csr.der
}

function sign_efi () {
    USER="$1"; shift

    # Sign the EFI binary
    ./sthsm-sign --testing --auth-key-label="${USER}" efi \
      --in scripts/testdata/efi.bin \
      --out efi.signed.bin \
      --cert-common-name="Test"
    
    pesigcheck --debug -n 0 -i efi.signed.bin -c cert.der
}