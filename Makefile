# command names
GO:=go
GOBINDIR:=$(shell go env GOPATH)/bin/
GOTEST="$(GOBINDIR)/gotestsum"

# build artifacts, sources
STHSM_SIGN:=cmd/sign
STHSM_CREDS:=cmd/creds
STHSM_DATA:=cmd/data

# build-time parameters, values
GO_ENV:=CGO_ENABLED=0 GOARCH=amd64
LDFLAGS:=-s -w $(LDFLAGS_EXTRA)
LDFLAGS-STATIC:=$(LDFLAGS) -extldflags "-static"

# suppress lots of legacy SCCS and RCS lookups
MAKEFLAGS += --no-builtin-rules 

.DEFAULT_GOAL:=all
.PHONY: all
all: linux

linux: deps sthsm-sign sthsm-creds sthsm-data

.PHONY: deps
deps:
	@echo "Fetching and installing dependencies.."
	@$(GO) mod download
	@$(GO) install gotest.tools/gotestsum@latest

sthsm-sign: $(STHSM_SIGN)
	$(GO_ENV) GOOS=linux   $(GO) build -C $(STHSM_SIGN) -ldflags '$(LDFLAGS-STATIC)' -o $(PWD)/$@

sthsm-creds: $(STHSM_CREDS)
	$(GO_ENV) GOOS=linux   $(GO) build -C $(STHSM_CREDS) -ldflags '$(LDFLAGS-STATIC)' -o $(PWD)/$@

sthsm-data: $(STHSM_DATA)
	$(GO_ENV) GOOS=linux   $(GO) build -C $(STHSM_DATA) -ldflags '$(LDFLAGS-STATIC)' -o $(PWD)/$@

.PHONY: clean
clean:
	rm -f sthsm-sign sthsm-creds sthsm-data coverage.txt

.PHONY: test
test: deps
	$(GOTEST) -- -coverprofile=coverage.txt -covermode=atomic ./...

.PHONY: integrationtests
integrationtests: deps
	./scripts/run_all.sh

.PHONY: install
install: linux
	sudo install -m 0755 sthsm-sign /usr/local/bin/
	sudo install -m 0755 sthsm-creds /usr/local/bin/
	sudo install -m 0755 sthsm-data /usr/local/bin/

# disable many builtin rules
.SUFFIXES:
