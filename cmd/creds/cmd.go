package main

import (
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"os"
	"os/exec"

	"git.glasklar.is/system-transparency/project/sthsm/pkg/hsm"
	"github.com/certusone/yubihsm-go/commands"
)

type context struct {
	Testing   bool
	Host      string
	Port      string
	Password  string
	AuthKeyID string
	Label     string
}

type versionCmd struct {
}

type listKeysCmd struct {
}

type getPubKeyCmd struct {
	ID  string `help:"The ID of the key in hex format." required:"true"`
	Out string `help:"The output file." default:"key.pub"`
}

type deleteKeyCmd struct {
	ID string `help:"The ID of the key in hex format."`
}

type deleteAllKeysCmd struct {
}

type listCertificatesCmd struct {
	Verbose bool `help:"Print more information about the certificates."`
}

type exportCertificateCmd struct {
	ID  string `help:"The ID of the certificate in hex format." required:"true"`
	Out string `help:"The output file." default:"cert.der"`
	PEM bool   `help:"Whether to export the certificate in PEM format."`
}

type deleteCertificateCmd struct {
	ID string `help:"The ID of the certificate in hex format." required:"true"`
}

type deleteAllCertificatesCmd struct {
}

type createCSRCmd struct {
	Out                string `help:"The output file." default:"csr.der"`
	CommonName         string `help:"The common name of the certificate." required:"true"`
	Organization       string `help:"The organization of the certificate." required:"true"`
	OrganizationalUnit string `help:"The organizational unit of the certificate." required:"true"`
	Country            string `help:"The country of the certificate." required:"true"`
	Locality           string `help:"The locality of the certificate." required:"true"`
	Province           string `help:"The province of the certificate." optional:"true"`
	StreetAddress      string `help:"The street address of the certificate." required:"true"`
	PostalCode         string `help:"The postal code of the certificate." required:"true"`
	KeyAlgorithm       string `help:"The algorithm of the key. Possible values are rsa2048, rsa3072, rsa4096, ed25519" default:"ed25519"`
	ID                 string `help:"The ID of the key in hex format." default:"0"`
	Format             string `help:"The format of the CSR. Possible values are hsm (HSM data structure)." default:"hsm"`
}

type createCertificateCmd struct {
	Out                string `help:"The output file." default:"cert.der"`
	CAFile             string `help:"Path to the CA certificate."`
	CAID               string `help:"ID of the CA certificate in HSM."`
	CommonName         string `help:"The common name of the certificate." required:"true"`
	Organization       string `help:"The organization of the certificate." required:"true"`
	OrganizationalUnit string `help:"The organizational unit of the certificate." required:"true"`
	Country            string `help:"The country of the certificate." required:"true"`
	Locality           string `help:"The locality of the certificate." required:"true"`
	Province           string `help:"The province of the certificate." optional:"true"`
	StreetAddress      string `help:"The street address of the certificate." required:"true"`
	PostalCode         string `help:"The postal code of the certificate." required:"true"`
	KeyAlgorithm       string `help:"The algorithm of the key. Possible values are rsa2048, rsa3072, rsa4096, ed25519" default:"ed25519"`
	CodeSigning        bool   `help:"Whether the certificate is used for code signing."`
	ID                 string `help:"The ID of the key in hex format." default:"0"`
	Store              string `help:"The store to use. Possible values are file, yubihsm." default:"yubihsm"`
}

type createSignKeyCmd struct {
	Label        string `help:"The label of the key." required:"true"`
	KeyAlgorithm string `help:"The algorithm of the key. Possible values are rsa2048, rsa3072, rsa4096, ed25519" default:"ed25519"`
}

var cli struct {
	Testing      bool   `help:"Enable test mode."`
	Host         string `help:"The host of the YubiHSM2." default:"localhost"`
	Port         string `help:"The port of the YubiHSM2." default:"12345"`
	AuthKeyID    string `help:"The domain of the YubiHSM2."`
	AuthKeyLabel string `help:"The label of the YubiHSM2."`
	Password     string `help:"The password of authentication key the YubiHSM2."`

	Version versionCmd `cmd help:"Prints the version of the program"`
	Key     struct {
		GenSign   createSignKeyCmd `cmd help:"Create a asymmetric signing key on the YubiHSM2."`
		Public    getPubKeyCmd     `cmd help:"Get the public key of a key on the YubiHSM2."`
		List      listKeysCmd      `cmd help:"List all asymmetric keys on the YubiHSM2."`
		Delete    deleteKeyCmd     `cmd help:"Delete a asymmetric key on the YubiHSM2."`
		DeleteAll deleteAllKeysCmd `cmd help:"Delete all asymmetric keys on the YubiHSM2."`
	} `cmd`
	CSR struct {
		Create createCSRCmd `cmd help:"Create a certificate signing request on the YubiHSM2."`
	} `cmd`
	Cert struct {
		Create    createCertificateCmd     `cmd help:"Create a certificate on the YubiHSM2."`
		Delete    deleteCertificateCmd     `cmd help:"Delete a certificate on the YubiHSM2."`
		DeleteAll deleteAllCertificatesCmd `cmd help:"Delete all certificates on the YubiHSM2."`
		List      listCertificatesCmd      `cmd help:"List all certificates on the YubiHSM2."`
		Export    exportCertificateCmd     `cmd help:"Export a certificate from the YubiHSM2."`
	} `cmd`
}

func conn(ctx *context) (*hsm.HSM, error) {
	var err error
	var yubiHSM *hsm.HSM
	if ctx.Label == hsm.ListKey.Label {
		ctx.Password = hsm.ListKeyPassword
	} else if ctx.Label == hsm.DefaultKey.Label {
		ctx.Password = hsm.DefaultKeyPassword
		ctx.AuthKeyID = hsm.KeyIDToString(hsm.DefaultKey.ID)
	}
	if ctx.Testing {
		testing := &hsm.TestingPass{}
		password, err := testing.GetPassword(ctx.Label, false)
		if err != nil {
			return nil, err
		}
		ctx.Password = string(password)
	} else if len(ctx.Password) == 0 {
		console := &hsm.ConsolePass{}
		password, err := console.GetPassword(ctx.Label, false)
		if err != nil {
			return nil, err
		}
		ctx.Password = string(password)
	}
	if len(ctx.AuthKeyID) != 0 {
		yubiHSM, err = hsm.NewConnection(ctx.Host, ctx.Port, ctx.Password, ctx.AuthKeyID)
		if err != nil {
			return nil, err
		}
	} else {
		fmt.Println("Looking for authentication key...")
		keyID, err := hsm.GetAuthKeyIDByLabel(ctx.Host, ctx.Port, ctx.Label)
		if err != nil {
			return nil, err
		}
		yubiHSM, err = hsm.NewConnection(ctx.Host, ctx.Port, ctx.Password, hsm.KeyIDToString(keyID))
		if err != nil {
			return nil, err
		}
	}
	return yubiHSM, nil
}

func (v *versionCmd) Run(ctx *context) error {
	fmt.Printf("%s\n", programVersion)
	return nil
}

func (l *listKeysCmd) Run(ctx *context) error {
	ctx.Label = hsm.ListKey.Label
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	keys, err := yubiHSM.ListKeys(commands.ObjectTypeAsymmetricKey)
	if err != nil {
		return err
	}

	fmt.Println()
	for _, key := range keys {
		fmt.Printf("ID: \t\t\t 0x%02x\n", key.ID)
		fmt.Printf("Domains: \t\t %s\n", hsm.DomainsToStrings(key.Domains))
		fmt.Printf("Label: \t\t\t \"%s\"\n", key.Label)
		fmt.Printf("Algorithm: \t\t %s\n", hsm.AlgoToString(key.Algorithm))
		fmt.Printf("Type: \t\t\t %s\n", hsm.TypeToString(key.Type))
		fmt.Printf("Capabilities: \t\t %s\n", hsm.CapabilitiesToStrings(key.Capabilities))
		fmt.Printf("Delegated Capabilities:  %s\n", hsm.CapabilitiesToStrings(key.DelegatedCapabilities))
		fmt.Println()
	}
	return nil
}

func (g *getPubKeyCmd) Run(ctx *context) error {
	ctx.Label = hsm.ListKey.Label
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	id, err := hsm.HexToID(g.ID)
	if err != nil {
		return err
	}
	data, _, err := yubiHSM.GetPubKey(id)
	if err != nil {
		return err
	}
	os.WriteFile(g.Out, data, 0644)
	return nil
}

func (d *deleteKeyCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	id, err := hsm.HexToID(d.ID)
	if err != nil {
		return err
	}
	return yubiHSM.DeleteAsymmetricKey(id)
}

func (d *deleteAllKeysCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	keys, err := yubiHSM.ListKeys(commands.ObjectTypeAsymmetricKey)
	if err != nil {
		return err
	}
	for _, key := range keys {
		if err := yubiHSM.DeleteAsymmetricKey(key.ID); err != nil {
			return err
		}
	}
	return nil
}

func (c *createCertificateCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	subject := hsm.PKIXNameByString(c.CommonName, c.Organization, c.OrganizationalUnit,
		c.Country, c.Locality, c.Province, c.StreetAddress, c.PostalCode)
	keyAlgo, err := hsm.KeyAlgorithmByString(c.KeyAlgorithm)
	if err != nil {
		return err
	}
	if len(c.CAFile) == 0 && len(c.CAID) == 0 {
		return fmt.Errorf("either --ca-file or --ca-id must be specified")
	}
	caID, err := hsm.HexToID(c.CAID)
	if err != nil {
		return err
	}
	var file []byte
	var ca *x509.Certificate
	if len(c.CAFile) > 0 {
		file, err = os.ReadFile(c.CAFile)
		if err != nil {
			return err
		}
		ca, err = x509.ParseCertificate(file)
		if err != nil {
			return err
		}
	} else if caID != 0 {
		ca, err = yubiHSM.LoadCertificate(caID)
		if err != nil {
			return err
		}
	}
	id, err := hsm.HexToID(c.ID)
	if err != nil {
		return err
	}
	var data []byte
	if c.CodeSigning {
		data, err = yubiHSM.NewCertificate(hsm.CodeSigning, ca, keyAlgo, subject, id)
		if err != nil {
			return err
		}
	} else {
		data, err = yubiHSM.NewCertificate(hsm.CodeSigning, ca, keyAlgo, subject, id)
		if err != nil {
			return err
		}
	}
	cert, err := x509.ParseCertificate(data)
	if err != nil {
		return err
	}
	if c.Store == "yubihsm" {
		certID, err := yubiHSM.StoreCertificate(cert)
		if err != nil {
			return err
		}
		fmt.Printf("Stored certificate in yubihsm with ID: 0x%02x\n", certID)
	} else if c.Store == "file" {
		if err := os.WriteFile(c.Out, data, 0644); err != nil {
			return err
		}
	} else {
		return fmt.Errorf("invalid store: %s", c.Store)
	}
	return nil
}

func (c *createCSRCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	subject := hsm.PKIXNameByString(c.CommonName, c.Organization, c.OrganizationalUnit,
		c.Country, c.Locality, c.Province, c.StreetAddress, c.PostalCode)
	keyAlgo, err := hsm.KeyAlgorithmByString(c.KeyAlgorithm)
	if err != nil {
		return err
	}
	id, err := hsm.HexToID(c.ID)
	if err != nil {
		return err
	}
	data, err := yubiHSM.NewCSR(keyAlgo, subject, id)
	if err != nil {
		return err
	}
	if c.Format == "hsm" {
		serialized, err := json.Marshal(data)
		if err != nil {
			return err
		}
		if err := os.WriteFile(c.Out, serialized, 0644); err != nil {
			return err
		}
	} else {
		return fmt.Errorf("invalid format: %s", c.Format)
	}
	return nil
}

func (l *listCertificatesCmd) Run(ctx *context) error {
	ctx.Label = hsm.ListKey.Label
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	certs, err := yubiHSM.ListCertificates()
	if err != nil {
		return err
	}
	for _, cert := range certs {
		fmt.Printf("ID: \t\t\t 0x%02x\n", cert.ID)
		fmt.Printf("Label: \t\t\t \"%s\"\n", cert.Label)
		fmt.Printf("Capabilities: \t\t %s\n", cert.Capabilities)
		fmt.Printf("Delegated Capabilities:  %s\n", cert.DelegatedCapabilites)
		if l.Verbose {
			der, err := yubiHSM.LoadCertificate(cert.ID)
			if err == nil {
				tmpDir, err := os.MkdirTemp("/tmp", "yubi-hsm-cert-")
				tmpFile := tmpDir + "/cert.der"
				if err == nil {
					if err := os.WriteFile(tmpFile, der.Raw, 0644); err == nil {
						cmd := exec.Command("openssl", "x509", "-inform", "DER", "-in", tmpFile, "-text")
						stdout, _ := cmd.CombinedOutput()
						fmt.Println(string(stdout))
					}
				}
				defer os.RemoveAll(tmpDir)
			}
		}
		fmt.Println()
	}
	return nil
}

func (e *exportCertificateCmd) Run(ctx *context) error {
	ctx.Label = hsm.ListKey.Label
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()

	id, err := hsm.HexToID(e.ID)
	if err != nil {
		return err
	}
	cert, err := yubiHSM.LoadCertificate(id)
	if err != nil {
		return err
	}
	if e.PEM {
		os.WriteFile(e.Out, pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: cert.Raw}), 0644)
		return nil
	}
	os.WriteFile(e.Out, cert.Raw, 0644)
	return nil
}

func (d *deleteCertificateCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()

	id, err := hsm.HexToID(d.ID)
	if err != nil {
		return err
	}
	return yubiHSM.RemoveCertificate(id)
}

func (d *deleteAllCertificatesCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()

	certs, err := yubiHSM.ListCertificates()
	if err != nil {
		return err
	}
	for _, cert := range certs {
		if err := yubiHSM.RemoveCertificate(cert.ID); err != nil {
			return err
		}
	}
	return nil
}

func (c *createSignKeyCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	keyAlgo, err := hsm.KeyAlgorithmByString(c.KeyAlgorithm)
	if err != nil {
		return err
	}
	keyID, err := yubiHSM.CreateSigningKey(c.Label, keyAlgo)
	if err != nil {
		return err
	}
	fmt.Printf("Created key with ID: 0x%02x\n", keyID)
	return nil
}
