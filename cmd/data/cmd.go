package main

import (
	"fmt"
	"os"

	"git.glasklar.is/system-transparency/project/sthsm/pkg/hsm"
)

type context struct {
	Testing   bool
	Host      string
	Port      string
	Password  string
	AuthKeyID string
	Label     string
}

type versionCmd struct {
}

type listDataCmd struct {
}

type getDataCmd struct {
	Label string `help:"The label of the data." required:"true"`
	Out   string `help:"The output file." default:"data.bin"`
}

type putDataCmd struct {
	Label string `help:"The label of the data." required:"true"`
	In    string `help:"The input file." required:"true"`
}

type deleteDataCmd struct {
	Label string `help:"The label of the data." required:"true"`
}

var cli struct {
	Testing      bool   `help:"Enable test mode."`
	Host         string `help:"The host of the YubiHSM2." default:"localhost"`
	Port         string `help:"The port of the YubiHSM2." default:"12345"`
	AuthKeyID    string `help:"The domain of the YubiHSM2."`
	AuthKeyLabel string `help:"The label of the YubiHSM2."`
	Password     string `help:"The password of authentication key the YubiHSM2."`

	Version versionCmd `cmd help:"Prints the version of the program"`
	Data    struct {
		List   listDataCmd   `cmd help:"List all data on the YubiHSM2."`
		Get    getDataCmd    `cmd help:"Get data from the YubiHSM2."`
		Put    putDataCmd    `cmd help:"Put data to the YubiHSM2."`
		Delete deleteDataCmd `cmd help:"Delete data from the YubiHSM2."`
	} `cmd`
}

func conn(ctx *context) (*hsm.HSM, error) {
	var err error
	var yubiHSM *hsm.HSM
	if ctx.Label == hsm.ListKey.Label {
		ctx.Password = hsm.ListKeyPassword
	} else if ctx.Label == hsm.DefaultKey.Label {
		ctx.Password = hsm.DefaultKeyPassword
		ctx.AuthKeyID = hsm.KeyIDToString(hsm.DefaultKey.ID)
	}
	if ctx.Testing {
		testing := &hsm.TestingPass{}
		password, err := testing.GetPassword(ctx.Label, false)
		if err != nil {
			return nil, err
		}
		ctx.Password = string(password)
	} else if len(ctx.Password) == 0 {
		console := &hsm.ConsolePass{}
		password, err := console.GetPassword(ctx.Label, false)
		if err != nil {
			return nil, err
		}
		ctx.Password = string(password)
	}
	if len(ctx.AuthKeyID) != 0 {
		yubiHSM, err = hsm.NewConnection(ctx.Host, ctx.Port, ctx.Password, ctx.AuthKeyID)
		if err != nil {
			return nil, err
		}
	} else {
		fmt.Println("Looking for authentication key...")
		keyID, err := hsm.GetAuthKeyIDByLabel(ctx.Host, ctx.Port, ctx.Label)
		if err != nil {
			return nil, err
		}
		yubiHSM, err = hsm.NewConnection(ctx.Host, ctx.Port, ctx.Password, hsm.KeyIDToString(keyID))
		if err != nil {
			return nil, err
		}
	}
	return yubiHSM, nil
}

func (v *versionCmd) Run(ctx *context) error {
	fmt.Printf("%s\n", programVersion)
	return nil
}

func (l *listDataCmd) Run(ctx *context) error {
	ctx.Label = hsm.ListKey.Label
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	data, err := yubiHSM.ListData()
	if err != nil {
		return err
	}
	for _, d := range data {
		fmt.Printf("ID: \t\t\t 0x%02x\n", d.ID)
		fmt.Printf("Type: \t\t\t %s\n", d.Type)
		fmt.Printf("Label: \t\t\t \"%s\"\n", d.Label)
		fmt.Printf("Length: \t\t %d\n", d.Length)
		fmt.Printf("Capabilities: \t\t %s\n", d.Capabilities)
		fmt.Printf("Delegated Capabilities:  %s\n", d.DelegatedCapabilites)
		fmt.Println()
	}
	return nil
}

func (g *getDataCmd) Run(ctx *context) error {
	ctx.Label = hsm.ListKey.Label
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	data, err := yubiHSM.GetData(g.Label)
	if err != nil {
		return err
	}
	os.WriteFile(g.Out, data, 0644)
	return nil
}

func (p *putDataCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	data, err := os.ReadFile(p.In)
	if err != nil {
		return err
	}
	return yubiHSM.PutData(p.Label, data)
}

func (d *deleteDataCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	return yubiHSM.RemoveData(d.Label)
}
