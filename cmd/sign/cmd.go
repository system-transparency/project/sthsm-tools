package main

import (
	"crypto/sha256"
	"fmt"
	"os"

	"git.glasklar.is/system-transparency/project/sthsm/pkg/hsm"
	"github.com/foxboron/go-uefi/efi/pecoff"
)

type context struct {
	Testing   bool
	Host      string
	Port      string
	Password  string
	AuthKeyID string
	Label     string
}

type versionCmd struct {
}

type signAuthenticodeCmd struct {
	In             string `help:"The input file." required:"true"`
	Out            string `help:"The output file." default:"signature.bin"`
	CertCommonName string `help:"The common name of the certificate to use for signing" required:"true"`
}

type signDetachedCmd struct {
	In  string `help:"The input file." required:"true"`
	Out string `help:"The output file." default:"signature.bin"`
	ID  string `help:"The ID of the key in hex format." required:"true"`
}

var cli struct {
	Testing      bool   `help:"Enable test mode."`
	Host         string `help:"The host of the YubiHSM2." default:"localhost"`
	Port         string `help:"The port of the YubiHSM2." default:"12345"`
	AuthKeyID    string `help:"The domain of the YubiHSM2."`
	AuthKeyLabel string `help:"The label of the YubiHSM2."`
	Password     string `help:"The password of authentication key the YubiHSM2."`

	Version  versionCmd          `cmd help:"Prints the version of the program"`
	EFI      signAuthenticodeCmd `cmd help:"Sign a EFI binary with a key on the YubiHSM2."`
	Detached signDetachedCmd     `cmd help:"Sign a file and output a detached signature with a key on the YubiHSM2."`
}

func conn(ctx *context) (*hsm.HSM, error) {
	var err error
	var yubiHSM *hsm.HSM
	if ctx.Label == hsm.ListKey.Label {
		ctx.Password = hsm.ListKeyPassword
	} else if ctx.Label == hsm.DefaultKey.Label {
		ctx.Password = hsm.DefaultKeyPassword
		ctx.AuthKeyID = hsm.KeyIDToString(hsm.DefaultKey.ID)
	}
	if ctx.Testing {
		testing := &hsm.TestingPass{}
		password, err := testing.GetPassword(ctx.Label, false)
		if err != nil {
			return nil, err
		}
		ctx.Password = string(password)
	} else if len(ctx.Password) == 0 {
		console := &hsm.ConsolePass{}
		password, err := console.GetPassword(ctx.Label, false)
		if err != nil {
			return nil, err
		}
		ctx.Password = string(password)
	}
	if len(ctx.AuthKeyID) != 0 {
		yubiHSM, err = hsm.NewConnection(ctx.Host, ctx.Port, ctx.Password, ctx.AuthKeyID)
		if err != nil {
			return nil, err
		}
	} else {
		fmt.Println("Looking for authentication key...")
		keyID, err := hsm.GetAuthKeyIDByLabel(ctx.Host, ctx.Port, ctx.Label)
		if err != nil {
			return nil, err
		}
		yubiHSM, err = hsm.NewConnection(ctx.Host, ctx.Port, ctx.Password, hsm.KeyIDToString(keyID))
		if err != nil {
			return nil, err
		}
	}
	return yubiHSM, nil
}

func (v *versionCmd) Run(ctx *context) error {
	fmt.Printf("%s\n", programVersion)
	return nil
}

func (s *signAuthenticodeCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	data, err := os.ReadFile(s.In)
	if err != nil {
		return err
	}
	id, err := yubiHSM.GetKeyIDByCertCommonName(s.CertCommonName)
	if err != nil {
		return err
	}
	key, err := yubiHSM.LoadKey(id)
	if err != nil {
		return err
	}
	cert, err := yubiHSM.GetCertByKeyID(id)
	if err != nil {
		return err
	}
	pe := pecoff.PECOFFChecksum(data)
	sig, err := pecoff.CreateSignature(pe, cert, key)
	if err != nil {
		return err
	}
	binary, err := pecoff.AppendToBinary(pe, sig)
	if err != nil {
		return err
	}
	os.WriteFile(s.Out, binary, 0644)
	return nil
}

func (s *signDetachedCmd) Run(ctx *context) error {
	yubiHSM, err := conn(ctx)
	if err != nil {
		return err
	}
	defer yubiHSM.Close()
	data, err := os.ReadFile(s.In)
	if err != nil {
		return err
	}
	id, err := hsm.HexToID(s.ID)
	if err != nil {
		return err
	}
	h := sha256.New()
	h.Write(data)
	digest := h.Sum(nil)
	sig, err := yubiHSM.Sign(id, digest)
	if err != nil {
		return err
	}
	os.WriteFile(s.Out, sig, 0644)
	return nil
}
