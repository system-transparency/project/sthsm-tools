package main

import (
	"github.com/alecthomas/kong"
)

const (
	programName    = "sthsm-sign"
	programDesc    = "System Transparency HSM signing tool"
	programVersion = "1.0"
)

func main() {
	ctx := kong.Parse(&cli,
		kong.Name(programName),
		kong.Description(programDesc),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: true,
			Summary: true,
		}))
	err := ctx.Run(&context{Testing: cli.Testing, Host: cli.Host, Port: cli.Port,
		Password: cli.Password, AuthKeyID: cli.AuthKeyID, Label: cli.AuthKeyLabel})
	ctx.FatalIfErrorf(err)
}
