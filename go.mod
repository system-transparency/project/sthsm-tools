module git.glasklar.is/system-transparency/project/sthsm-tools

go 1.20

require (
	git.glasklar.is/system-transparency/project/sthsm v0.0.0-20230628123744-bb345c3b178e
	github.com/alecthomas/kong v0.8.0
	github.com/certusone/yubihsm-go v0.3.0
	github.com/foxboron/go-uefi v0.0.0-20230218004016-d1bb9a12f92c
)

require (
	github.com/enceve/crypto v0.0.0-20160707101852-34d48bb93815 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/afero v1.9.5 // indirect
	golang.org/x/crypto v0.10.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
	golang.org/x/term v0.9.0 // indirect
	golang.org/x/text v0.10.0 // indirect
)
